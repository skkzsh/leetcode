#
# @lc app=leetcode id=20 lang=python3
#
# [20] Valid Parentheses
#

# @lc code=start
class Solution:
    def isValid(self, s: str) -> bool:
        right_stack: List[str] = []

        for c in s:
            if c == "(":
                right_stack.append(")")
            elif c == "[":
                right_stack.append("]")
            elif c == "{":
                right_stack.append("}")
            elif len(right_stack) > 0 and c == right_stack[-1]:
                right_stack.pop()
            else:
                return False

        if len(right_stack) > 0:
            return False

        return True

        
# @lc code=end

