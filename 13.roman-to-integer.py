#
# @lc app=leetcode id=13 lang=python3
#
# [13] Roman to Integer
#

# @lc code=start
class Solution:
    def romanToInt(self, s: str) -> int:
        value_table = {
            'I': 1,
            'V': 5,
            'X': 10,
            'L': 50,
            'C': 100,
            'D': 500,
            'M': 1000,
        }

        sum = 0

        for i, c in enumerate(s[:-1]):
            if value_table[s[i]] >= value_table[s[i + 1]]:
               sum += value_table[s[i]]
            else:
               sum -= value_table[s[i]]

        return sum + value_table[s[-1]]

# @lc code=end

