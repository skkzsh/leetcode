#
# @lc app=leetcode id=1480 lang=python3
#
# [1480] Running Sum of 1d Array
#

# @lc code=start
class Solution:
    def runningSum(self, nums: List[int]) -> List[int]:
        sum = 0
        resultList: List[int] = []
        for i in range(len(nums)):
            sum += nums[i]
            resultList.append(sum)
        return resultList

        # return [sum(nums[: i + 1]) for i in(range(len(nums)))]

# @lc code=end

