#
# @lc app=leetcode id=9 lang=python3
#
# [9] Palindrome Number
#

# @lc code=start
class Solution:
    def isPalindrome(self, x: int) -> bool:
        x_list = []

        y = x
        while y > 0:
            x_list.append(y % 10)
            y //= 10

        x_reversed = 0
        for i, z in enumerate(reversed(x_list)):
            x_reversed += z * (10 ** i)

        return x == x_reversed
        
# @lc code=end

