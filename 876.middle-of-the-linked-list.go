/*
 * @lc app=leetcode id=876 lang=golang
 *
 * [876] Middle of the Linked List
 */

// @lc code=start
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func middleNode(head *ListNode) *ListNode {
	mid := head
	for tail := head; tail != nil && tail.Next != nil; tail = tail.Next.Next {
		mid = mid.Next
	}
	return mid
}

// @lc code=end

