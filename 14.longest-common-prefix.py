#
# @lc app=leetcode id=14 lang=python3
#
# [14] Longest Common Prefix
#

# @lc code=start
class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        count = 0
        for s in zip(*strs):
            if len(set(s)) == 1:
                count += 1
            else:
                return strs[0][:count]
        
# @lc code=end

