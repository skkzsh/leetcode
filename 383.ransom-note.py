#
# @lc app=leetcode id=383 lang=python3
#
# [383] Ransom Note
#

# @lc code=start
class Solution:
    def canConstruct(self, ransomNote: str, magazine: str) -> bool:
        init_table = {c: 0 for c in list('abcdefghijklmnopqrstuvwxyz')}
        magazine_table = init_table.copy()
        ransom_table = init_table.copy()

        for c in magazine:
            magazine_table[c] += 1
        for c in ransomNote:
            ransom_table[c] += 1
            if ransom_table[c] > magazine_table[c]:
                return False
        return True

# @lc code=end

