#
# @lc app=leetcode id=1 lang=python3
#
# [1] Two Sum
#

# @lc code=start
class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        for i, n in enumerate(nums):
            remain = nums[i + 1 : ]
            m = target - n
            if m in remain:
                return [i, i + 1 + remain.index(m)]

# @lc code=end

